import { expect } from 'chai';
import sinon from 'sinon';
import request from 'request';

import day5 from '../src/day5';
  
describe('day 5 test', () => {
  describe('using spies', () => {
    let spyRequestGet;

    beforeEach(() => {
      spyRequestGet = sinon.spy(request, 'get');
    });

    afterEach(() => {
      spyRequestGet.restore();
    });

    it('should make a GET request once', (done) => {
      day5('expressjs', 'express', (data) => {
        expect(spyRequestGet.callCount)
          .to.equal(1);
        done();
      });
    });

    it('should make request with expected URL', (done) => {
      day5('expressjs', 'express', (data) => {
        expect(spyRequestGet.getCall(0).args[0].uri)
          .to.equal('https://api.github.com/repos/expressjs/express');
        done();
      });
    });
  });

  describe('using stub', () => {
    let stubRequestGet;

    // running before the test suites
    before(() => {

    });

    // running after the test suites
    after(() => {

    });

    // running before each test
    beforeEach(() => {
      stubRequestGet = sinon.stub(request, 'get');
    });

    // running after each test
    afterEach(() => {
      stubRequestGet.restore();
    });
    
    it('should make once GET request', (done) => {
      stubRequestGet.yields(
        null,
        {statusCode: 200},
        {stargazers_count: 100}
      );

      day5('expressjs', 'express', (data) => {
        expect(stubRequestGet.callCount)
          .to.equal(1);
        done();
      });
    });

    it('should return expected data', (done) => {
      const givenAPIResponse = {
        'stargazers_count': 100
      };

      stubRequestGet.yields(
        null,
        {statusCode: 200},
        givenAPIResponse
      );

      day5('expressjs', 'express', (data) => {
        expect(data).to.deep.equal({
          stars: givenAPIResponse.stargazers_count
        });
        done();
      });
    });

    it('should return expected data when rate limited', (done) => {
      const givenAPIResponse = {
        'message': 'API rate limit exceeded',
        'documentation_url': 'https://developer.github.com/v3/#rate-limiting'
      };

      stubRequestGet.yields(
        new Error('API rate limit exceeded'),
        {statusCode: 403},
        givenAPIResponse
      );

      day5('expressjs', 'express', (data) => {
        expect(data).to.deep.equal({
          success: false,
          statusCode: 403,
          error: 'API is rate limited - try again later'
        });
        done();
      });
    });
  });
});